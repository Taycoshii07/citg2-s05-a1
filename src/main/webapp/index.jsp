<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>JSP Activity</title>
</head>
<body>

    <!-- Using the scriptlet tag, create a variable dateTime -->
    <!-- Use the LocalDateTime and DateTimeFormatter classes -->
    <!-- Change the pattern to "yyyy-MM-dd HH:mm:ss" -->

    <%!
        private int initVar = 3;
        private int serviceVar = 3;
        private int destroyVar = 3;
    
        public void jspInit() {
            initVar--;
            System.out.println("jspInit(): init" + initVar);
        }

        public void jspDestroy() {
            destroyVar--;
            destroyVar = destroyVar + initVar;
            System.out.println("jspDestroy(): destroy" + destroyVar);
            
        }
    %>
    <%
		 	serviceVar--;
		 	System.out.println("__jspService(): service" +serviceVar);
		 	String content1 = "content1 : " +initVar;
		 	String content2 = "content2 : " +serviceVar;
		 	String content3 = "content3 : " +destroyVar;
	%>

    <% 
        java.time.LocalDateTime dateTime = java.time.LocalDateTime.now();
        java.time.format.DateTimeFormatter formatter = 
        java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    %>
    
    <!-- Using the date time variable declared above, print out the time values -->
  	<!-- Manila = currentDateTime -->
  	<!-- Japan = +1 -->
  	<!-- Germany = -7 -->
  	<!-- You can use the "plusHours" and "minusHours" method from the LocalDateTime class -->

    <h1>Our Date and Time now is...</h1>
    <ul>
        <li> Manila: <%= dateTime.format(formatter) %> </li>
        <li> Japan: <%= dateTime.plusHours(1).format(formatter) %> </li>
        <li> Germany: <%= dateTime.minusHours(7).format(formatter) %> </li>
    </ul>

    <h1>JSP</h1>
    <p><%= content1 %></p>
    <p><%= content2 %></p>
    <p><%= content3 %></p>

</body>
</html>